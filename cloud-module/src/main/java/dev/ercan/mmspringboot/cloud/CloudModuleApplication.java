package dev.ercan.mmspringboot.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudModuleApplication.class, args);
	}

}
