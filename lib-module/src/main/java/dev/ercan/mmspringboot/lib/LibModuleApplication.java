package dev.ercan.mmspringboot.lib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibModuleApplication.class, args);
	}

}
